<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HackathonController extends Controller
{
    /**
    * Converts string parameter by changing the capitalization of the characters
    * @param string
    * @return number_identity
    */    
    public function alterCapitalization($string){

	    $lower_cased_string = mb_strtolower($string); 

	    $converted_string = preg_replace_callback(
        '#[aeiou\s]+#i',
	        function ($matches) {
	            return strtoupper($matches[0]);
	        }, $lower_cased_string );

		return $converted_string;
    }

    /**
    * Function to identify if a number is even or odd
    * @param decimal number
    * @return number_identity
    * Wh
    */

    public function identifyEvenOrOdd($decimal_number){
       
    	if($decimal_number % 2 == 0){ 
             $number_identity = "Even";  
	    } else {
	    	$number_identity = "Odd";
	    }
	    return $number_identity;
    }

}