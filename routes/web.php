<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/count-dir-files/{path_dir}', 'HackathonController@countDirectoryFiles');

Route::get('/alter-capitalization/{string}', 'HackathonController@alterCapitalization');

Route::get('/identify-even-odd/{decimal_number}', 'HackathonController@identifyEvenOrOdd');

Route::get('/string-lower/{string}', 'HackathonController@consonantsToLowercase');

Route::get('/string-upper/{string}', 'HackathonController@vowelsToUppercase');